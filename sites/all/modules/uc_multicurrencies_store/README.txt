UBERCART MULTI-CURRENCIES STORE
=========================
This module integrates currency module with ubercart store.

Install
=========================
1) Simply drop this uc_multicurrencies_store folder into the modules directory (/sites/all/modules/).
2) Install dependencies (uc_payment and currency). 
3) Install via admin/build/modules.

Ubercart Multi-currencies Store settings
=========================
1) Go to Configure basic store settings (admin/store/settings/store) and configure Currency format:
  * Select your Default currency, 
  * Check currencies allow in your site.

Author
=========================
Juan Vizcarrondo (http://drupal.org/user/352652). 
