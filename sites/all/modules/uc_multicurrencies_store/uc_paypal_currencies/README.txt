PAYPAL MULTI-CURRENCIES PAYMENT
=========================
This module Allows  paypal mult-currencies payment.

Install
=========================
1) Simply drop this uc_multicurrencies_store folder into the modules directory (/sites/all/modules/).
2) Install dependencies (uc_payment, uc_multicurrencies_store and uc_paypal). 
3) Install via admin/build/modules.

Paypal Multi-currencies Payment
=========================
You should add at least one currency accepted by paypal in Currency allows (admin/store/settings/store), 
  WARNING: when the user Select a currency that is not accepted by paypal gateway will not be available. If your only gateway is paypal, you should allow only currencies accepted by paypal (https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_currency_codes).

Author
=========================
Juan Vizcarrondo (http://drupal.org/user/352652). 
