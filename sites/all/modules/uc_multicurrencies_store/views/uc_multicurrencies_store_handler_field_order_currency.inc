<?php

/**
 * @file
 * Views handler: Product price field.
 */

/**
 * Returns a formatted price value to display in the View.
 */
class uc_multicurrencies_store_handler_field_order_currency extends views_handler_field {

  /**
   * Overrides views_handler::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['format'] = array('default' => 'uc_currency_symbol');

    return $options;
  }

  /**
   * Overrides views_handler::options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = $this->options;

    $form['format'] =  array(
      '#title' => t('Format'),
      '#type' => 'radios',
      '#options' => array(
        'uc_currency_name' => t('Currency Name'),
        'uc_currency_symbol' => t('Currency Symbol'),
        'string' => t('Abreviation'),
      ),
      '#default_value' => $options['format'],
      '#weight' => 1,
    );
  }

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    if ($this->options['format'] == 'string') {
      return parent::render($values);
    }
    $translate = array(
      'uc_currency_name' => 'name',
      'uc_currency_symbol' => 'symbol',
    );
    $format = $this->options['format'];
    if ($format == 'uc_currency_name' || $format == 'uc_currency_symbol') {
      $currencies = uc_multicurrencies_store_get_currencies();
      $value = $this->get_value($values);
      
      if (is_null($value) || ($value == '') || !count($currencies)) {
        return '';
      }
      $new_value = $currencies[$value][$translate[$format]] ? $currencies[$value][$translate[$format]] : variable_get('currency_default_from', 'USD');
      return $new_value;
    }
  }
}
