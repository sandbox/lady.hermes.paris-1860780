<?php

/**
 * Filter by currency.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_currency extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    $currencies = uc_multicurrencies_store_get_currencies();
    foreach ($currencies as $id => $currency) {
      $this->value_options[$id] = $currency['name'];
    }
  }
}
