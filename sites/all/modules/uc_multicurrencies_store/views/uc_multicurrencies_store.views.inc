<?php

/**
 * Implements hook_views_data_alter().
 */
function uc_multicurrencies_store_views_data_alter(&$data) {
  //products
  $data['uc_products']['list_price']['field']['handler'] = 'uc_product_handler_field_currency_price';
  $data['uc_products']['cost']['field']['handler'] = 'uc_product_handler_field_currency_price';
  $data['uc_products']['sell_price']['field']['handler'] = 'uc_product_handler_field_currency_price';
  //orders
  $data['uc_orders']['order_total']['field']['handler'] = 'uc_order_handler_field_money_currency_amount';
  $data['uc_orders']['order_total']['field']['additional fields'] = array(
    'field' => 'currency',
  );
  $data['uc_orders']['order_total_cost']['field']['handler'] = 'uc_order_handler_field_money_currency_amount';
  $data['uc_orders']['order_total_cost']['field']['additional fields'] = array(
    'field' => 'currency',
  );
  //Add currency field
  $data['uc_orders']['currency'] = array(
    'title' => t('Currency'),
    'help' => t('The currency of the order.'),
    'field' => array(
      'handler' => 'uc_multicurrencies_store_handler_field_order_currency',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_currency',
    ),
  );
  //order products
  $data['uc_order_products']['price']['field']['handler'] = 'uc_order_handler_field_money_currency_amount';
  $data['uc_order_products']['price']['field']['additional fields'] = array(
    'currency' => array(
      'table' => 'uc_orders',
      'field' => 'currency',
    ),
  );
  $data['uc_order_products']['cost']['field']['handler'] = 'uc_order_handler_field_money_currency_amount';
  $data['uc_order_products']['cost']['field']['additional fields'] = array(
    'currency' => array(
      'table' => 'uc_orders',
      'field' => 'currency',
    ),
  );
}

