(function ($) {

Drupal.behaviors.initModalFormsCartView = {
  attach: function (context, settings) {
    
    $("a[href*='/cart'], a[href*='?q=cart']", context).once('init-modal-forms-cart-view', function () {
      this.href = this.href.replace(/cart,'modal_forms/nojs/cart_view');
    }).addClass('ctools-use-modal ctools-modal-modal-popup-small');
  }
};

})(jQuery);
