(function ($) {
    Drupal.behaviors.sgrid = {
      attach: function (context, settings) {
             $( ".sortable ul" ).sortable({
                update: function(event, ui) {
                    var ItemOrder = $(this).sortable('toArray').toString();   
                    $('#sgridorder').val(ItemOrder);
                    $(this).removeClass('sgrid-line-end'); 
                    sgrid_reset_eol($(this));
                },
                drag: function(event, ui) {
                    reset_eol($(this));                  
                },
                helper: 'clone',
                disabled: !(Drupal.settings.sgrid.sort_allowed)                                                
             });
            $( ".sortable ul li" ).each(function (index, Element) {                    
                    $(this).attr('id', 'sgrid_item_' + $(this).find('span.sgrid-nid').text());
            });            
            // Initial order            
            var sort_items = []; 
            $( ".sortable ul li" ).each(function (index, Element) {
                id = 'sgrid_item_' + $(this).find('span.sgrid-nid').text();
                $(this).attr('id', id);
                sort_items[index] = id;
            });                  
            $('#sgridorder').val(sort_items.toString());                
            $( ".sortable ul li" ).disableSelection();
      }
    }
    
    function sgrid_reset_eol(obj) {
        // Reset end of line classes
        obj.find('li').each(function (index, Element) {
            $(this).removeClass('sgrid-line-end');
            var eof = index / Drupal.settings.sgrid.row_length;
            if (eof == parseInt(eof)) {
                $(this).addClass('sgrid-line-end');                           
            }
        });           
    }   
})(jQuery);

