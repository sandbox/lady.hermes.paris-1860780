<?php

/**
 * @file
 * This file is the default customer invoice template for Ubercart.
 *
 * Available variables:
 * - $products: An array of product objects in the order, with the following
 *   members:
 *   - title: The product title.
 *   - model: The product SKU.
 *   - qty: The quantity ordered.
 *   - total_price: The formatted total price for the quantity ordered.
 *   - individual_price: If quantity is more than 1, the formatted product
 *     price of a single item.
 *   - details: Any extra details about the product, such as attributes.
 * - $line_items: An array of line item arrays attached to the order, each with
 *   the following keys:
 *   - line_item_id: The type of line item (subtotal, shipping, etc.).
 *   - title: The line item display title.
 *   - formatted_amount: The formatted amount of the line item.
 * - $shippable: TRUE if the order is shippable.
 *
 * Tokens: All site, store and order tokens are also available as
 * variables, such as $site_logo, $store_name and $order_first_name.
 *
 * Display options:
 * - $op: 'view', 'print', 'checkout-mail' or 'admin-mail', depending on
 *   which variant of the invoice is being rendered.
 * - $business_header: TRUE if the invoice header should be displayed.
 * - $shipping_method: TRUE if shipping information should be displayed.
 * - $help_text: TRUE if the store help message should be displayed.
 * - $email_text: TRUE if the "do not reply to this email" message should
 *   be displayed.
 * - $store_footer: TRUE if the store URL should be displayed.
 * - $thank_you_message: TRUE if the 'thank you for your order' message
 *   should be displayed.
 *
 * @see template_preprocess_uc_order()
 */
?>
<table width="800px" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="" style="font-family: sans-serif, fantasy; font-size: small;">
  <tr>
    <td>
      <table width="800px" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF" style="font-family: sans-serif, fantasy; font-size: small;">
       
        <tr valign="top" style="text-align:center">
          <td>

            <font style="text-align:center;font-size:34px">EM PARIS BOUTIQUE</font></td></tr>
<tr style="text-align:center"><td><font style="font-size:small">151 ĐỒNG KHỔI STREET - SAIGON <br> SINGAPORE <br>+84 98 272 2866 - +33 777370685 </font></font></td></tr>
 
	    
            <?php if (isset($order->data['new_user'])): ?>
            <p><b><?php print t('An account has been created for you with the following details:'); ?></b></p>
            <p><b><?php print t('Username:'); ?></b> <?php print $order_new_username; ?><br />
            <b><?php print t('Password:'); ?></b> <?php print $order_new_password; ?></p>
        
            <p><b><?php print t('Want to manage your order online?'); ?></b><br />
            <?php print t('If you need to check the status of your order, please visit our home page at !store_link and click on "My account" in the menu or login with the following link:', array('!store_link' => $store_link)); ?>
            <br /><br /><?php print $site_login_link; ?></p>
            <?php endif; ?>
	               <table cellpadding="4" cellspacing="0" border="0" width="800px" style="font-family: verdana, arial, helvetica; font-size: small;">
	                        
             <!--   <tr>
                <td nowrap="nowrap">
                  <b><?php print t('E-mail Address:'); ?></b>
                </td>
              <td width="98%">
                  <?php print $order_email; ?>
                </td>
              </tr> -->

              <tr>
                <td colspan="2">`

                  <table width="800px" cellspacing="0" cellpadding="0" style="margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
                    <tr>
                 <!--     <td valign="top" width="50%">
                        <b><?php print t('Billing Address:'); ?></b><br />
                        <?php print $order_billing_address; ?><br />
                        <br />
                        <b><?php print t('Billing Phone:'); ?></b><br />
                        <?php print $order_billing_phone; ?><br />
                      </td> -->
                      <?php if ($shippable): ?>
                      <td valign="top" width="50%">
                                               <?php print $order_shipping_address; ?><br />
                        <br />
                                              <?php print $order_shipping_phone; ?><br />
                      </td>
		      <td valign="top" width="50%">
			 <?php print t('Order #:');  print $order_link; ?> <br />
                                                                      <?php print $order_created; ?>
                         
                      </td>

                      <?php endif; ?>
                    </tr>
                  </table>

                </td>
              </tr>
                          
              <tr>
               <td colspan="2" bgcolor="white" style="/* color: white; *//* border-top-style: solid; *//* border-color: black; *//* border-top-width: thin; */margin: 10%;padding-left: 80px;">
                 <table style="border-top-width:thin;border-top-style:solid;border-top-color: black;width: 550px;"></table>
  
                </td>              </tr>

              <?php if ($shippable): ?>
                           <?php endif; ?>

              <tr>
                <td colspan="2">

                  <table border="0" bor cellpadding="1" cellspacing="0" width="800px" style="margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
                      <tr>
                     
                        <table width="550px" style="margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
  			    <tr style="text-align:left">
                            <th valign="top" width="300px" nowrap="nowrap">
			    Item                             
                            </td>
 			    <th>QTY </th>
                            <th width="65px">
                             <b>Price</b>
                             
                                                     </th>
                          </tr>
                          <?php foreach ($products as $product): ?>
                          <tr>
                            <td valign="top" width="300px" nowrap="nowrap">
                              <?php print $product->title; ?> <br> <b> <?php if ($product->model != na)  print $product->model; ?> </b>
                            </td>
 			    <td> <?php print $product->qty; ?> </td>
                            <td width="65px">
                             <b> <?php print $product->total_price; ?></b>
                              <?php print $product->individual_price; ?><br />
                                                     </td>
                          </tr>
                          <?php endforeach; ?>
                        </table>

                      </td>
                    </tr>
			
                      <tr>
                         <td colspan="2">  

				<table width="550px" style="margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
  			  <th width="300px">
   <td>Subtotal:</td>
                      <td width="65px">
                        <?php print $order_subtotal; ?>
                      </td>

                          </th>
                                                  <tr>
                            <td valign="top" width="300px" nowrap="nowrap">
                                                         </td>
 			                        </td>

                                                                                                         </tr>
                            </table>


			</td>
                    </tr>

                    <?php foreach ($line_items as $item): ?>
                    <?php if ($item['type'] == 'subtotal' || $item['type'] == 'total')  continue; ?>

                    <tr>
                      <td colspan="2">  

				<table width="550px" style="margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
  			  <th width="300px">
   <td> <?php print $item['title']; ?>:
</td>
                      <td width="65px">
                        <?php print $item['formatted_amount']; ?>

                      </td>

                          </th>
                                                  <tr>
                            <td valign="top" width="300px" nowrap="nowrap">
                                                         </td>
 			                        </td>

                                                                                                         </tr>
                            </table>


			</td>

                      <td colspan="2">
                                             </td>
                      <td>
                                             </td>
                    </tr>

                    <?php endforeach; ?>

                   
                    <tr>
                      <td colspan="2">  

				<table width="550px" style="margin-top:80px;margin-left:10%;font-family: verdana, arial, helvetica; font-size: small;">
  			  <th width="300px">
   <td> <b><?php print t('Grand Total:'); ?>&nbsp;</b>
</td>
                      <td width="65px">
                         <b><?php print $order_total; ?></b>

                      </td>

                          </th>
                                                  <tr>
                            <td valign="top" width="300px" nowrap="nowrap">
                                                         </td>
 			                        </td>

                                                                                                         </tr>
                            </table>


			</td>

                      </tr>

                  </table>

                </td>
              </tr>

              <?php if ($help_text || $email_text || $store_footer): ?>
              <tr>
                <td colspan="2">
                  <hr noshade="noshade" size="1" /><br />

                  <?php if ($help_text): ?>
                  <p><b><?php print t('Where can I get help with reviewing my order?'); ?></b><br />
                  <?php print t('To learn more about managing your orders on !store_link, please visit our <a href="!store_help_url">help page</a>.', array('!store_link' => $store_link, '!store_help_url' => $store_help_url)); ?>
                  <br /></p>
                  <?php endif; ?>

                  <?php if ($email_text): ?>
                  <p><?php print t('Please note: This e-mail message is an automated notification. Please do not reply to this message.'); ?></p>

                  <p><?php print t('Thanks again for shopping with us.'); ?></p>
                  <?php endif; ?>

                  <?php if ($store_footer): ?>
                  <p><b><?php print $store_link; ?></b><br /><b><?php print $site_slogan; ?></b></p>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endif; ?>

            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>


