<?php

/**
 * @file
 * Theme functions for the uc_cart module.
 */

/**
 * Themes the shopping cart block title.
 *
 * @param $variables
 *   An associative array containing:
 *   - title: The text to use for the title of the block.
 *   - icon_class: Class to use for the cart icon image or FALSE if the icon is
 *     disabled.
 *   - collapsible: TRUE or FALSE indicating whether or not the cart block is
 *     collapsible.
 *   - collapsed: TRUE or FALSE indicating whether or not the cart block is
 *     collapsed.
 *
 * @ingroup themeable
 */
function theme_uc_multicurrencies_store($variables) {
  $title = $variables['title'];
  $icon_class = $variables['icon_class'];
  $collapsible = $variables['collapsible'];
  $collapsed = $variables['collapsed'];

  $output = '';

  // Add in the cart image if specified.
  if ($icon_class) {
    $output .= theme('uc_cart_block_title_icon', array('icon_class' => $icon_class));
  }

  // Add the main title span and text, with or without the arrow based on the
  // cart block collapsibility settings.
  if ($collapsible) {
    $output .= '<span class="cart-block-title-bar" title="' . t('Show/hide shopping cart contents.') . '">' . $title;
    if ($collapsed) {
      $output .= '<span class="cart-block-arrow arrow-down"></span>';
    }
    else {
      $output .= '<span class="cart-block-arrow"></span>';
    }
    $output .= '</span>';
  }
  else {
    $output .= '<span class="cart-block-title-bar">' . $title . '</span>';
  }

  return 
