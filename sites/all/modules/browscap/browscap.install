<?php
/**
 * @file
 * Install, update and uninstall functions for the Browscap module.
 */

/**
 * Implements hook_install().
 */
function browscap_schema() {
  $schema['browscap'] = array(
    'fields' => array(
      'useragent' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'data' => array(
        'type' => 'blob',
        'size' => 'big',
      ),
      'propertyname' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'agentid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'masterparent' => array(
        'type' => 'int',
        'size' => 'tiny',
      ),
      'litemode' => array(
        'type' => 'int',
        'size' => 'tiny',
      ),
      'parent' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'comment' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'browser' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'version' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'majorver' => array(
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'minorver' => array(
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'platform' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'platform_version' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'platform_description' => array(
        'type' => 'text',
      ),
      'alpha' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'beta' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'win16' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'win32' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'win64' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'frames' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'iframes' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'tables' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'cookies' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'backgroundsounds' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'javascript' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'vbscript' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'javaapplets' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'activexcontrols' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'ismobiledevice' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'issyndicationreader' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'crawler' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'cssversion' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'aolversion' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'device_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'device_maker' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'renderingengine_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'renderingengine_version' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
      'renderingengine_description' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
      ),
    ),
    'primary key' => array('useragent'),
    'indexes' => array(
      'parent' => array('parent'),
    ),
  );
  $schema['cache_browscap'] = drupal_get_schema_unprocessed('system', 'cache');

  return $schema;
}

/**
 * Implements hook_install().
 */
function browscap_install() {
  // Update the browscap information
  _browscap_import();

  // Record when the browscap information was updated
  variable_set('browscap_imported', REQUEST_TIME);
}

/**
 * Implements hook_uninstall().
 */
function browscap_uninstall() {
  variable_del('browscap_imported');
  variable_del('browscap_version');
  variable_del('browscap_enable_automatic_updates');
  variable_del('browscap_automatic_updates_timer');
}

/**
 * Drop the unused Browscap 1.x statistics table.
 * Create the required fields in the schema.
 * Remove the last import setting to force refresh.
 */
function browscap_update_7200() {
  db_drop_table('browscap_statistics');
  variable_del('browscap_imported');
  variable_del('browscap_version');
  
  $fields = array(
    'propertyname' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
    ),
    'agentid' => array(
      'type' => 'int',
      'unsigned' => TRUE,
    ),
    'masterparent' => array(
      'type' => 'int',
      'size' => 'tiny',
    ),
    'litemode' => array(
      'type' => 'int',
      'size' => 'tiny',
    ),
    'parent' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'comment' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'browser' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'version' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'majorver' => array(
      'type' => 'int',
      'unsigned' => TRUE,
    ),
    'minorver' => array(
      'type' => 'int',
      'unsigned' => TRUE,
    ),
    'platform' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'platform_version' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'platform_description' => array(
      'type' => 'text',
    ),
    'alpha' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'beta' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'win16' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'win32' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'win64' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'frames' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'iframes' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'tables' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'cookies' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'backgroundsounds' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'javascript' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'vbscript' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'javaapplets' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'activexcontrols' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'ismobiledevice' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'issyndicationreader' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'crawler' => array(
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ),
    'cssversion' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'aolversion' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'device_name' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'device_maker' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'renderingengine_name' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'renderingengine_version' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
    'renderingengine_description' => array(
      'type' => 'varchar',
      'length' => 255,
      'default' => '',
    ),
  );
  
  // Loop through the new fields and add them
  foreach ($fields as $name => $field) {
    db_add_field('browscap', $name, $field);
  }
  // Add the parent index
  db_add_index('browscap', 'parent', array('parent'));
}
