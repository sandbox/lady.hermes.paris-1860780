<?php

/**
 * @file
 * Admin pages and forms for user settings.
 *
 */

// TODO: add pages to view data in the fb_user_app table.

/**
 * Form builder; Configure settings for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function fb_allowed_users_form($form, &$form_state) {
  $options = array(0 => t('<none>')) + fb_admin_get_app_options(FALSE);
  if (count($options) == 1) {
    $message = t('You must create an app first!');
    drupal_set_message($message, 'error');
    return array('help' => array('#markup' => $message));
  }
 if (!isset($fbu)) {
    $fbu = fb_facebook_user();
  }
  $uids = array();
  
  $form['fb_allowed_users'] = array(
    '#type' => 'markup',
    '#markup' => print_r(fb_get_friends($fbu, NULL)),
     '#prefix' => '<pre>',
    '#suffix' => '</pre>',
  );
  return confirm_form($form,
                      t('Are you sure you set exclusive members for'),
                      isset($_GET['destination']) ? $_GET['destination'] : FB_PATH_ADMIN_APPS,
                      t('This action cannot be undone.'),
                      t('Sync Friends List'),
                      t('Cancel')
  );
}

/**
 * Button submit function.  Use has clicked delete, send them to confirm page.
 */
function fb_allowed_users_form_submit($form, &$form_state) {
  if (!isset($fbu)) {
    $fbu = fb_facebook_user();
  }

 $fbu = fb_get_friends($fbu, NULL);

   foreach($fbu as $item)
   {
      db_merge('fb_allowed_user')
	->key(array('fbu' => $item))
	->fields(array( 'fbu' => $item, ))
	->execute();
   }
}

